define([
    'jquery',
    'jquery-ui',
    'slick',
    'domReady!'
], function ($) {
    'use strict';
    /*$('#sorter').selectmenu({
        change: function( event, ui ) {
            $(this).trigger('change');
        },
        position: { my : "left-100 center+70", at: "right center" }
    });*/

    $('.in-logos').slick({
        arrows: false,
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        variableWidth: false,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    centerMode: true,
                    centerPadding: '0px',
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 425,
                settings: {
                    centerMode: true,
                    centerPadding: '0px',
                    slidesToShow: 2
                }
            }
        ]
    });

    $('.panel.header > .header.links').clone().appendTo('#store\\.links');
});
