define([
    'jquery',
    'uiComponent',
    'underscore',
    'Magento_Customer/js/customer-data',
    'Magento_Ui/js/modal/modal',
    'mage/translate'
], function($, Component, _, customerData, $t) {
    'use strict';

    var mixin = {
        initialize: function () {
            this._super();
            this.initRegistries();
            this.addGiftRegistries();
        },

        addGiftRegistries: function (elem) {
            var textTop = [];

            $(".registries").hide();

            $(".registry-list").prepend('<p id="drop-top">Choose your Gift List</p>');

            $("body").on('click', "#drop-top",function() {
                $(this).toggleClass('new-icon-drop');
                $(".registries").slideToggle('fast');
            });

            $('body').on("click",".registries input + span", function () {
                $(this).toggleClass('bold-text');
                var element = $(this).html(), index = textTop.indexOf(element);
                if(index!=-1) {
                    textTop.splice(index, 1);
                }
                else {
                    textTop.push(element);
                }

                $('#drop-top').text(textTop.join(", "));

            });
        },

        defineBehaviour: function(data, event) {
            if(!this.isLoggedIn()){
                event.stopPropagation();
                window.location.href = this.loginUrl;
            }
            this.initRegistries();

            if (this.registries().length == 1) {
                event.stopPropagation();
                this.addProduct();
            }
        }

    };

    return function (target) {
        return target.extend(mixin);
    };

});
