define([
    'jquery',
    'jquery-ui',
    'domReady!'
], function ($) {
    'use strict';

    $.widget('mage.selectmenuWidget', {
        options:{
            position:{
                my: "left-100 center+70",
                at: "right center"
            }
        },
        _create:function(element){

            var _opt=this.options;
            var _self=this.element;

            $(_self).selectmenu({
                change: function() {
                    $(this).trigger('change');
                },
                position: _opt.position
            });
        }
    });
    return $.mage.selectmenuWidget;
});
